package com.yitong.fasdmain.config;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
public class FasdOssSecurityConfig {
    @ConditionalOnProperty(prefix = "fasd-oss.security",name = "enabled",havingValue = "true")
    @Configuration
    class SecurityConfig  {

        @ConditionalOnProperty(prefix = "fasd-oss.security",name = "login",havingValue = "web")
        @EnableWebSecurity
        @Configuration
        class SecurityWebConfig extends WebSecurityConfigurerAdapter {

            @Override
            protected void configure(HttpSecurity http) throws Exception {

                http.formLogin()
                        .loginPage( "/login.html" )
                        .loginProcessingUrl( "/loginaccount" )
                        .and ()

                        .authorizeRequests ().filterSecurityInterceptorOncePerRequest (true)
                        .antMatchers( "/login","/login.html","/loginaccount" ).permitAll()
                        .antMatchers( "/**" ).authenticated()
                        .and()
                        .csrf().disable()
                        .cors ().disable ()
                        .requestCache ().and ()
                        .headers ().xssProtection().block(false)
                        .xssProtectionEnabled(true)
                        .and()
                        .frameOptions ().sameOrigin ().configure (http);
            }

            @Override
            public void configure(WebSecurity web) {
//将项目中静态资源路径开放出来
                web.ignoring().antMatchers("/assets/**","/**/assets/**","/config/**", "/css/**", "/fonts/**", "/img/**", "/js/**","/**.html");
            }
        }
        @ConditionalOnProperty(prefix = "fasd-oss.security",name = "login",havingValue = "basic")
        @EnableWebSecurity
        @Configuration
        class SecurityBasicConfig extends WebSecurityConfigurerAdapter {

            @Override
            protected void configure(HttpSecurity http) throws Exception {

                http.httpBasic()
                        .and ()

                        .authorizeRequests ().filterSecurityInterceptorOncePerRequest (true)
                        .antMatchers( "/login","/login.html","/loginaccount" ).permitAll()
                        .antMatchers( "/**" ).authenticated()
                        .and()
                        .csrf().disable()
                        .cors ().disable ()
                        .requestCache ().and ()
                        .headers ().xssProtection().block(false)
                        .xssProtectionEnabled(true)
                        .and()
                        .frameOptions ().sameOrigin ().configure (http);
            }

            @Override
            public void configure(WebSecurity web) {
//将项目中静态资源路径开放出来
                web.ignoring().antMatchers("/assets/**","/**/assets/**","/config/**", "/css/**", "/fonts/**", "/img/**", "/js/**","/**.html");
            }
        }
    }
    @ConditionalOnProperty(prefix = "fasd-oss.security",name = "enabled",havingValue = "false")
    @EnableWebSecurity
    @Configuration
    class SecurityConfigNO extends WebSecurityConfigurerAdapter {

        @Override
        protected void configure(HttpSecurity http) throws Exception {
            http.cors().disable().csrf().disable()
                    .cors ().disable ()
                    .requestCache ().and ()
                    .headers ().xssProtection().block(false)
                    .xssProtectionEnabled(true)
                    .and()
                    .frameOptions ().sameOrigin ().configure (http);
        }


    }
}